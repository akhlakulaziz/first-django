from django.http import HttpResponse #untuk import keluaran respon
from django.shortcuts import render #untuk mengembalikan nilai dari html template

def index(request): #ini menggunakan import render

    context = {
        'judul' : 'Home',
        'pesan' : 'Lorem ipsum dolor sit amet',
        'nav' :[ #cara menambahkan link lewat views
            ['/','Home'],
            ['/blog','Blog'], #/blog adalah url link, dan Blog adalah nama link yang akan tampil
            ['/about','About'],
        ],
        'banner' : "img/banner_home.png",
    }
    return render(request, 'index.html', context)

def about(request): #ini menggunakan import HttpResponse
    halo = "ini about boys"
    return HttpResponse(halo) 