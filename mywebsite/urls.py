"""mywebsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .views import index, about #ini untuk memanggil function index dan about dari file views
from . import views #memanggil semua method yang ada di views mywebsite
from blog import views as blogViews#memanggil semua method yang ada di views app blog
from about import views as aboutViews

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index), # ini cara penulisan route index, views.index artinya kita memanggil method index dari file views
    path('about/', include('about.urls')), #ini cara penulisan halaman route selain home
    path('blog/', include('blog.urls')),
]
