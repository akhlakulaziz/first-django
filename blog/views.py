from django.shortcuts import render
from django.shortcuts import HttpResponse
from .models import blog #untuk memanggil database pada tabel post


def index(request): #ini menggunakan import render
    blogs = blog.objects.all() #memanggil semua isi dari tabel post
    # print(posts) #print post
    context = {
        'judul' : 'Blog',
        'pesan' : 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus commodi molestiae provident.',
        'nav' :[ #cara menambahkan link lewat views
            ['/','Home'],
            ['/blog','Blog'], #/blog adalah url link, dan Blog adalah nama link yang akan tampil
            ['/about','About'],
        ],
        'item' : blogs,

    }
    return render(request, 'blog/index.html', context)

def news(request): #ini menggunakan import render
    context = {
        'judul' : 'news',
        'pesan' : 'sandra',
    }
    return render(request, 'blog/index.html', context)

def cerita(request): #ini menggunakan import render
    context = {
        'judul' : 'cerita',
        'pesan' : 'otong',
    }
    return render(request, 'blog/index.html', context)