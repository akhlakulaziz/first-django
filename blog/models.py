from django.db import models
from django.db.models.base import Model

class blog (models.Model):
    title = models.CharField(max_length=255, blank=True) # blank=True adalah bahwa field tersebut boleh null 
    body = models.TextField()
    email = models.EmailField(default='nama@gamil.com') # default adaah nilai awalnya
    alamat = models.CharField(max_length=200, blank=True)
    waktu_posting = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{}".format(self.title) #untuk mengubah tabelnya menjadi nama title 
# Create your models here.
