from django.shortcuts import render

def index(request): #ini menggunakan import render
    context = {
        'judul' : 'About',
        'pesan' : 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus commodi molestiae provident.',
        'nav' :[ #cara menambahkan link lewat views
            ['/','Home'],
            ['/blog','Blog'], #/blog adalah url link, dan Blog adalah nama link yang akan tampil
            ['/about','About'],
        ],
    }
    return render(request, 'about/index.html', context)
    # return render(request, 'about.html')
# Create your views here.
